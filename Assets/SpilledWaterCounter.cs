using System.Collections;
using System.Collections.Generic;
using Obi;
using UnityEngine;

public class SpilledWaterCounter : MonoBehaviour
{
    [SerializeField] private ObiSolver solver;
    [SerializeField] private int count = 0;
    private ObiCollider ObiCollider;
    private int hundredsCounter = 0;

    void Start()
    {
        solver.OnCollision += Solver_OnCollision;
        ObiCollider = GetComponent<ObiCollider>();
    }

    private void Solver_OnCollision(ObiSolver s, ObiSolver.ObiCollisionEventArgs e)
    {
        var world = ObiColliderWorld.GetInstance();
        foreach (Oni.Contact contact in e.contacts)
        {
            var col = world.colliderHandles[contact.bodyB].owner;
            if (ObiCollider == col)
            {
                count++;
                if (count > 2000) GameController.OnLose.Invoke();
                if (count / 100 >= hundredsCounter)
                {
                    hundredsCounter++;
                    CuttablePaper.OnHundredParticlesSpill.Invoke(hundredsCounter);
                }
            }
        }
    }
}