using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject paper;
    [SerializeField] private GameObject cuttablePaper;
    [SerializeField] private GameObject cup;
    [SerializeField] private float speed;
    [SerializeField] private List<GameObject> prizes;
    [SerializeField] private List<Transform> lines;
    [SerializeField] private Animator player;
    [SerializeField] private Animator girl;
    private bool playing = true;
    public static Action OnLose;
    public static Action onWin;
    private bool takeInput;


    private void Start()
    {
        OnLose += CutPaper;
        Invoke(nameof(EnableInput), 2f);
    }

    void EnableInput()
    {
        takeInput = true;
    }

    private void Update()
    {
        if (!playing) return;
        if (Input.GetMouseButton(0) && takeInput)
        {
            paper.transform.Translate(Vector3.forward * (Time.deltaTime * speed), Space.World);
            cup.transform.Translate(Vector3.forward * (Time.deltaTime * speed));
        }

        if (cup.transform.position.z > lines[lines.Count - 1].position.z)
        {
            playing = false;
            //onWin.Invoke();
            Win();
            Debug.Log("You win!");
        }
    }

    void Win()
    {
        GivePrize(lines.Count - 1);
        player.SetTrigger("Win");
        Debug.Log("You win!");
    }

    void CutPaper()
    {
        if (!playing) return;
        girl.SetTrigger("Slap");
        playing = false;
        for (int i = lines.Count - 1; i >= 0; i--)
        {
            if (cup.transform.position.z > lines[i].transform.position.z)
            {
                GivePrize(i);
                break;
            }
        }

        cuttablePaper.transform.parent = null;
    }

    void GivePrize(int place)
    {
        prizes[place * 2].transform.position += 1f * transform.up;
        prizes[place * 2 + 1].transform.position += 1f * transform.up;
    }
}