using System;
using UnityEngine;

public class CuttablePaper : MonoBehaviour
{
    public static Action<int> OnHundredParticlesSpill;
    private Material material;

    private void Start()
    {
        OnHundredParticlesSpill += ChangeColor;
        material = GetComponent<Renderer>().material;
    }

    void ChangeColor(int count)
    {
        byte color = Convert.ToByte(255 - count * 5);
        material.color = new Color32(color, color, color, 255);
    }
}